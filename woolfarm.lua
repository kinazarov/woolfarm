local r = require("robot")
local components = require("component")
local pc = require("computer")
local tb = components.tractor_beam
local rechargeTime = 6

local function trymove()
	local success = false
	repeat
		success = r.forward()
		os.sleep(0.25)
	until success
end

local function gu(steps)
	--goUse
    for counter = 1, (steps or 1) do
        trymove()
        r.useDown()
        tb.suck()
    end
end

local function tl()
	r.turnLeft()
end

local function tr()
	r.turnRight()
end

local function unloadWoolAndReturn(halfWigth)
	for slotNumber=1,r.inventorySize() do
		r.select(slotNumber)
		r.drop()
	end
	r.select(1)
	tl()
	for i = 2, halfWigth * 2 do
		trymove()
	end
	tl()
end

local function runWithScissors(length, halfWigth)
	for counter = 1, halfWigth do
		gu(length)
		tl()
		gu()
		tl()
		gu(length)
        if counter < halfWigth then
            tr()
            gu()
            tr()
        end
	end
end

local stopper = false

local length = 10
local width = 8
local halfWidth = width // 2
local waitForNewWoolTime = 8

repeat
	runWithScissors(length, halfWidth)
	unloadWoolAndReturn(halfWidth)
    --recharge
    os.sleep(rechargeTime)
	stopper = r.detectUp()
    os.sleep(waitForNewWoolTime)
until stopper

pc.shutdown()
